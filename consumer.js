// const kafka = require('kafka-node');
const config = require('./config');
const socketIO = require('socket.io');
const express = require('express');
const http = require('http');
const cors = require('cors');
// const producerLib = require('./produce');
const app = express();
const port = process.env.PORT || '8888';
// const Producer = kafka.Producer;
// const client = new kafka.KafkaClient({ kafkaHost: config.KafkaHost });
//const producer = new Producer(client, { requireAcks: 0, partitionerType: 2 });
const Influx = require('influxdb-nodejs');
const clientInflux = new Influx('http://venti-dev.albot.io:8086/ventilator');

// clientInflux.queryRaw('select time, Cstat, ExpMinVol, MinVol, OXYGEN, PEEP, PIP, PPEAK, Paw, PetCO2, Pulse, Rinsp, Spo2, TimeStamp, VTE, distinct(VentilatorID), fTotal from "ventilator_data"  order By time desc').then(data=>{
//   console.log(data)
// })
function emitData(id) {
  return new Promise((resolve, reject) => {
    // console.log(id)
    clientInflux.query('ventilator_data').where('VentilatorID', id).then(data => {
      let data2 = {...data,...{}}
      if (data.results.length && data.results[0].series.length && data.results[0].series[0]) {
        let values = data.results[0].series[0].values.sort((a, b) => new Date(a[0]).getTime() - new Date(b[0]).getTime());
        data.results[0].series[0].values = values[values.length - 1]
        
        if (global.SOCKET && values.length) {
          // console.log(JSON.stringify(data.results[0].series[0]))
          global.SOCKET.emit('new-data', data.results[0].series[0]);
        }
        resolve(data.results[0].series[0])
      } else {
        resolve({})
      }
    })
  });
}
function getVentilators() {
  clientInflux.queryRaw('select distinct(VentilatorID) from "ventilator_data"  order By time desc').then(async data => {
    // console.log(data.results[0].series[0].values)
    for (let i = 0; i < data.results[0].series[0].values.length; i++) {
      await emitData(data.results[0].series[0].values[i][1])
    }
    setTimeout(() => {
      getVentilators()
    }, 120000)
  })
}
app.use(cors({
  'allowedHeaders': ['Authorization', 'Content-Type', 'enctype', 'Accept'],
  'origin': '*',
  'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
  'preflightContinue': false
}));
app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});
app.set('port', port);
app.all('/', (req, res) => {
  res.send('Welcome')
});
// app.all('/produce', (req, res) => {
//   // console.log(req)
//   producerLib.pushDataToKafka(req.query.data || req.body.data, producer)
//     .then(result => {
//       console.log('res', result)
//       res.status(200).json({
//         meta: {
//           code: result.errorCode,
//           message: 'Successfull',
//           currentDate: new Date().toISOString()
//         },
//         // pagination: result.pagination,
//         // totalCount: result.totalCount,
//         // additional: result.additional,
//         // count: result.count,
//         data: result.data,
//         // extraData: result.extraData ? result.extraData : undefined
//       })
//     }).catch(error => {
//       console.log('error=>', error)
//       res.status(200).json({
//         meta: {
//           code: error.errorCode,
//           message: error.message,
//           currentDate: new Date().toISOString()
//         },
//         // pagination: result.pagination,
//         // totalCount: result.totalCount,
//         // additional: result.additional,
//         // count: result.count,
//         // data: result.data,
//         // extraData: result.extraData ? result.extraData : undefined
//       })
//     });

//   // res.send('Welcome')
// });
const server = http.createServer(app);
global.IO = socketIO(server, {
  cors: {
    origin: '*',
  }
});
// getVentilators()
IO.on('connection', (socket) => {
  global.SOCKET = socket;
  console.log('connected')
  global.SOCKET.on('ReadWriteParamTopic', data => {
    producerLib.pushDataToKafka(data, producer, 'ReadWriteParamTopic')
  });
  getVentilators()
});
// try {
//   const Consumer = kafka.Consumer;
//   const client = new kafka.KafkaClient({ idleConnection: 24 * 60 * 60 * 1000, kafkaHost: config.KafkaHost });
//   // console.log(client)
//   let consumer = new Consumer(
//     client,
//     [{ topic: config.KafkaTopic, partition: 0 }],
//     {
//       autoCommit: true,
//       fetchMaxWaitMs: 1000,
//       fetchMaxBytes: 1024 * 1024,
//       encoding: 'utf8',
//       // fromOffset: false
//     }
//   );
//   consumer.on('message', async function (message) {
//     let data;
//     try {
//       data = JSON.parse(message.value)
//     } catch {
//       data = message.value
//     }
//     console.log(
//       'kafka ',
//       JSON.stringify(data),
//       new Date()
//     );
//     if (global.SOCKET) {
//       global.SOCKET.emit('kafka', data);
//     }
//   })
//   consumer.on('error', function (error) {
//     //  handle error 
//     console.log('error', error);
//   });
// }
// catch (error) {
//   // catch error trace
//   console.log(error);
// }

// producer.on('ready', async function () {

// })
server.listen(port, function () {
  console.log(`API running on localhost: ${port}`);
});
