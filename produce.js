const Kafka = require('kafka-node');
const config = require('./config');

// const Producer = Kafka.Producer;
// const client = new Kafka.KafkaClient({ kafkaHost: config.KafkaHost });
// const producer = new Producer(client, { requireAcks: 0, partitionerType: 2 });



const pushDataToKafka = (dataToPush, producerLib, topic) => {
  return new Promise((resolve, reject) => {
    try {
      let payloadToKafkaTopic = [{ topic: topic || config.KafkaTopic, messages: typeof dataToPush == 'string' ? dataToPush : JSON.stringify(dataToPush) }];
      // payloadToKafkaTopic = [{ topic: config.KafkaTopic, messages:' JSON.stringify(dataToPush)' }]
      console.log('payloadToKafkaTopic', payloadToKafkaTopic);
      // producerLib.on('ready', async function () {
      // console.log('ready')
      producerLib.send(payloadToKafkaTopic, (err, data) => {
        console.log(err, 'data: ', data);
        resolve({ data })
      });

      producerLib.on('error', function (err) {
        //  handle error cases here
        console.log('err', err)
        reject(err);
      })
      // })
    }
    catch (error) {
      console.log('error', error)
      // reject(error);
    }
  });
};

module.exports.pushDataToKafka = pushDataToKafka
// const jsonData = require('./app_json.js');

// pushDataToKafka(jsonData);
